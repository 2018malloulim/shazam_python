import tkinter
from tkinter.filedialog import askopenfilename, askdirectory
from database import create_database_sql, suggestions_sql
from song_player import start_player, load_song, start_song, stop_song

#################################   Fonctions   #################################

def remove_duplicates(list) : # Cette fonction enlève les éléments redondants dans une liste
    
    new_list = []
    for element in list :
        if element not in new_list :
            new_list.append(element)
    return new_list

def play_song(list_suggestions, select_song) :
    song_path=list_suggestions[int(select_song.get())][1]
    load_song(song_path)
    start_song()

def popup(n_songs, list_suggestions) : # Cette fonction fait apparaître une fenêtre popup contenant les suggestions
    
    fInfos = tkinter.Toplevel() # Popup -> Toplevel()
    fInfos.title('Infos')
    taille = "500x" + str(250 + 17*min(len(list_suggestions), n_songs))#On configure la taille de la fenêtre en fonction du nombre de suggestions
    fInfos.geometry(taille)
    str_suggestions = ""
    if len(list_suggestions) < n_songs-1 : # On met un warning au cas où l'utilisateur demande plus de suggestions que la base ne peut en donner
        n_songs = len(list_suggestions)
        str_suggestions = "Pas suffisament de musiques similaires dans la base de données\n"
    for k in range(n_songs) :
        str_suggestions = str_suggestions + "\n" + str(k) + ' - ' + list_suggestions[k][0]
    tkinter.Label(fInfos, text=str_suggestions).pack(padx=10, pady=10)
    start_player()
    select_song=tkinter.StringVar()
    select_song.set(5)
    select_song_box=tkinter.Entry(fInfos, width = 10, textvariable=select_song).pack(padx=10, pady=10)
    tkinter.Button(fInfos, text='Play', command = lambda: play_song(list_suggestions, select_song)).pack(padx=10, pady=10)
    tkinter.Button(fInfos, text='Stop', command=stop_song).pack(padx=10, pady=10)
    tkinter.Button(fInfos, text='Quitter', command=fInfos.destroy).pack(padx=10, pady=10)

def tk_read_file() : # Cette fonction permet de sélectionner le fichier à analyser

    global file_path
    input_file_path = askopenfilename(parent=root, title="Select File") # Ouvre l'explorateur de fichiers
    file_path = input_file_path.rstrip('\n') # Prend la valeur de l'emplacement du fichier
    print(file_path)

def tk_select_directory() : # Cette fonction permet de sélectionner le dossier qui contient les musiques à partir desquelles on crée la base de donnée
    
    global directory_path
    input_directory_path = askdirectory(parent=root, title="Select Directory") # Ouvre l'explorateur de fichiers
    directory_path = input_directory_path.rstrip('\n') # Prend la valeur de l'emplacement du dossier
    print(directory_path)

def create_database() : # Cette fonction crée la base de donnée
    
    create_database_sql(directory_path)

def suggest_songs() : # Cette fonction crée la liste des suggestions

    n_songs = int(suggestions.get()) # Prend le nombre de suggestions indiqué par l'utilisateur dans l'interface
    lists_suggestions = suggestions_sql(file_path) # Fait la requête à la base de donnée qui donne toutes les musiques similaires
    list_suggestions = remove_duplicates(lists_suggestions[0] + lists_suggestions[1] + lists_suggestions[2]) # Enlève les musiques en double
    popup(n_songs,list_suggestions) # Ouvre la fenêtre popup

def quit(root) : # Cette fonction ferme la fenêtre
    
    root.destroy()

#################################   Interface   #################################

root = tkinter.Tk() # Crée la fenêtre principale (root)
top = tkinter.Frame(root)
top.pack(side='top')

root.minsize(400, 300)
root.geometry("700x300") # Configure la taille de root

root.title("Tkinter interface for shazam_python")

################################################################################

database_creation_label = tkinter.Label(top, text='Creating song database')
database_creation_label.grid(row=0, column=0, pady = 15)

################################################################################

button_select = tkinter.Button(top, text="Select songs directory", command=tk_select_directory, fg='green')
button_select.config(height = 2, width = 20 )
button_select.grid(row=1, column=0, pady=5)

button_database = tkinter.Button(top, text="Create database", command=create_database, fg='blue')
button_database.config(height = 2, width = 20 )
button_database.grid(row=1, column=1, pady=5)

################################################################################

hwtext=tkinter.Label(top,text='The input file must be a .mp3 file')
hwtext.grid(row=2, column=0, padx = 10, pady=10)

database_creation_label = tkinter.Label(top, text='Suggesting songs based on similarities')
database_creation_label.grid(row=2, column=1, padx = 10, pady=15)

suggestions_label=tkinter.Label(top, text='Number of suggestions')
suggestions_label.grid(row=2, column=2, pady=5)


################################################################################

button_read = tkinter.Button(top, text="Read Input File", command=tk_read_file, fg='green')
button_read.config(height = 2, width = 15 )
button_read.grid(row=3, column=0, pady=5)

button_suggest = tkinter.Button(top, text="Suggest songs", command=suggest_songs, fg='green')
button_suggest.config(height = 2, width = 15 )
button_suggest.grid(row=3, column=1, pady=5)

suggestions=tkinter.StringVar()
suggestions.set(5)
suggestions_box=tkinter.Entry(top, width = 10, textvariable=suggestions)
suggestions_box.grid(row=3, column=2, pady=5)

################################################################################

button_quit=tkinter.Button(top, text="Quit", command=lambda root=root:quit(root))
button_quit.config(height = 2, width = 15 )
button_quit.grid(row=4, column=2, padx=20,pady=5)

################################################################################

root.mainloop() # Appelle l'event loop