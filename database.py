import sqlite3
from tags import get_title, get_genre, get_artist, get_album
from os import listdir
from os.path import isfile, join

def create_database_sql(directory) : # Cette fonction permet de créer une base de données des musiques à partir d'un dossier (prend l'adresse du dossier en entrée)

    songdb = sqlite3.connect('song.db')
    cursor = songdb.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS SONG( songid integer PRIMARY KEY AUTOINCREMENT,title TEXT,artist TEXT,album TEXT,genre TEXT,path TEXT)""")

    song_tags = [f for f in listdir(directory) if isfile(join(directory, f))] # On crée une liste de tous les fichiers dans le dossier indiqué en entrée

    songs_list = []
    for songs in song_tags : # On cree une liste de tuples contenant les informations de chaque musique
        title = get_title(directory + '/' + songs) # Les informations sont prises dans les tags ID3 du fichier .mp3 grâce aux fonctions du fichier tags.py
        artist = get_artist(directory + '/' + songs)
        album = get_album(directory + '/' + songs)
        genre = get_genre(directory + '/' + songs)
        path = directory + '/' + songs
        if artist == None :
            artist ='unkown'
        if album == None :
            album ='unkown'
        if genre == None :
            genre ='unkown'
        songs_list.append((title, artist, album, genre, path))

    cursor.executemany(""" INSERT INTO SONG(title,artist,album,genre,path) VALUES(?,?,?,?,?) """, songs_list) # On remplit la table SONG avec les musiques

    songdb.commit()
    songdb.close()


def suggestions_sql(filepath) : # Cette fonction prend en entrée l'adresse d'une musique et renvoie la liste des musiques similaires dans la base de donnnée (provenant du même album, du même artiste et/ou étant du même genre)
    
    artist1 = get_artist(filepath)
    album1 = get_album(filepath)
    genre1 = get_genre(filepath)
    if artist1 == None :
        artist1 ='unkown'
    if album1 == None :
        album1 ='unkown'
    if genre1 == None :
        genre1 ='unkown'
    songdb = sqlite3.connect('song.db')
    cursor = songdb.cursor()
    
    cursor.execute("""SELECT artist, title, path FROM SONG WHERE album=?""", (album1,))
    l1= cursor.fetchall()
    listalbum=[]
    for L in l1 :
        if len(L)==3 :
            listalbum.append((L[0] + ' - ' + L[1],L[2]))

    cursor.execute("""SELECT artist, title, path FROM SONG WHERE artist=?""", (artist1,))
    l2 = cursor.fetchall()
    listartist = []
    for L in l2 :
        if len(L)==3 :
            listartist.append((L[0] + ' - ' + L[1],L[2]))

    cursor.execute("""SELECT artist, title, path FROM SONG WHERE genre=?""", (genre1,))
    l3 = cursor.fetchall()
    listgenre=[]
    for L in l3 :
        print(L)
        if len(L)==3 :
            listgenre.append((L[0] + ' - ' + L[1],L[2]))
        
    songdb.commit()
    songdb.close()

    return(listalbum,listartist,listgenre) # On retourne 3 listes : les musiques provenant du même album, les musiques provenant du même artiste, les musiques étant du même genre