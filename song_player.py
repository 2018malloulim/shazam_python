import pygame.mixer

def start_player() :
    pygame.mixer.init()

def load_song(song_path) :
    pygame.mixer.music.load(song_path)

def start_song() :
    pygame.mixer.music.play()

def stop_song() :
    pygame.mixer.music.stop()