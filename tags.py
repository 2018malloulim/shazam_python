from mutagen.easyid3 import EasyID3

# Ces fonctions permettent de lire les tags d'un fichier mp3

def get_title(filepath) :
    mp3info = EasyID3(filepath)
    if 'title' in mp3info :
        return mp3info['title'][0]
    else :
        index = filepath.rfind('/')
        return filepath[index:]

def get_genre(filepath) :
    mp3info = EasyID3(filepath)
    if 'genre' in mp3info :
        return mp3info['genre'][0]

def get_artist(filepath) :
    mp3info = EasyID3(filepath)
    if 'artist' in mp3info :
        return mp3info['artist'][0]

def get_album(filepath) :
    mp3info = EasyID3(filepath)
    if 'album' in mp3info :
        return mp3info['album'][0]